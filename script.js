// let usersFetch = "https://ajax.test-danit.com/api/json/users";
// let postsFetch = "https://ajax.test-danit.com/api/json/posts";
// const getResponse = (url) => fetch(url).then((response) => response.json());
// showCard();
// class Card {
//   constructor(postsId, title, body, email, fullName) {
//     this.postsId = postsId;
//     this.title = title;
//     this.body = body;
//     this.email = email;
//     this.fullName = fullName;
//   }
//   render() {
//     this.posts = document.querySelector(".card");
//     this.post = document.querySelector(".card__content").cloneNode(true);
//     this.posts.appendChild(this.post);
//     this.createCard();
//   }
//   delete() {
//     getResponse(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
//       method: "DELETE",
//     }).then(this.post.remove().then(event.preventDefault()));
//   }
//   createCard() {
//     this.postTitle = this.post.querySelector(".title");
//     this.postTitle.innerHTML = this.title;
//     this.postBody = this.post.querySelector(".text");
//     this.postBody.innerHTML = this.body;
//     this.autorData = this.post.querySelector(".person");
//     this.post.querySelector(".name").innerHTML = this.fullName;
//     this.post.querySelector(".username").innerHTML = this.email;
//     this.post
//       .querySelector(".icon")
//       .addEventListener("click", this.delete.bind(this));
//   }
// }
// function showCard() {
//   Promise.all([getResponse(postsFetch), getResponse(usersFetch)]).then(
//     ([posts, users]) => {
//       posts.filter(({ id: postId, userId, title, body }) => {
//         users.filter(({ id, name: fullName, email }) => {
//           if (id === userId) {
//             const card = new Card(postId, title, body, fullName, email);
//             card.render();
//           }
//         });
//       });
//     }
//   );
// }
let usersFetch = "https://ajax.test-danit.com/api/json/users";
let postsFetch = "https://ajax.test-danit.com/api/json/posts";

const getResponse = (url) => fetch(url).then((response) => response.json());
showCard();

class Card {
  constructor(postId, title, body, fullName, email) {
    this.postId = postId;
    this.title = title;
    this.body = body;
    this.fullName = fullName;
    this.email = email;
  }

  render() {
    this.posts = document.querySelector(".cards");
    this.content = document.querySelector(".card-content").cloneNode(true); //цепочка дубликатов
    this.posts.appendChild(this.content);
    this.createCard();
  }

  delete() {
    getResponse(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
      method: "DELETE",
    })
      .then(this.content.remove())
      .then(event.preventDefault());
  }

  createCard() {
    this.postTitle = this.content.querySelector(".title");
    this.postTitle.innerHTML = this.title;
    this.text = this.content.querySelector(".text");
    this.text.innerHTML = this.body;
    this.authorData = this.content.querySelector(".unn");
    this.content.querySelector(".name").innerHTML = this.fullName;
    this.content.querySelector(".username").innerHTML = this.email;
    this.content
      .querySelector(".img")
      .addEventListener("click", this.delete.bind(this));
  }
}
function showCard() {
  Promise.all([getResponse(postsFetch), getResponse(usersFetch)]).then(
    ([posts, users]) => {
      posts.filter(({ id: postId, userId, title, body }) => {
        users.filter(({ id, name: fullName, email }) => {
          if (id === userId) {
            const card = new Card(postId, title, body, fullName, email);
            card.render();
          }
        });
      });
    }
  );
}
